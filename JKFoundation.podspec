#
# Be sure to run `pod lib lint JKFoundation.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'JKFoundation'
  s.version          = '0.1.5'
  s.summary          = 'A short description of JKFoundation.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/iOS11luchao/JKFoundation'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'iOS11luchao' => '860665461@qq.com' }
  s.source           = { :git => 'https://github.com/iOS11luchao/JKFoundation.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'JKFoundation/Classes/**/*'
  
   s.resource_bundles = {
     'JKFoundation' => ['JKFoundation/Assets/*.png']
   }

  s.public_header_files = 'Pod/Classes/**/*.h' #public
  s.frameworks = 'UIKit'# , 'MapKit'
  s.dependency 'YYModel'
#  s.dependency 'Masonry'

# s.dependency 'AFNetworking' #, '~> 2.3'
end
