# JKFoundation

[![CI Status](http://img.shields.io/travis/iOS11luchao/JKFoundation.svg?style=flat)](https://travis-ci.org/iOS11luchao/JKFoundation)
[![Version](https://img.shields.io/cocoapods/v/JKFoundation.svg?style=flat)](http://cocoapods.org/pods/JKFoundation)
[![License](https://img.shields.io/cocoapods/l/JKFoundation.svg?style=flat)](http://cocoapods.org/pods/JKFoundation)
[![Platform](https://img.shields.io/cocoapods/p/JKFoundation.svg?style=flat)](http://cocoapods.org/pods/JKFoundation)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

JKFoundation is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'JKFoundation'
```

## Author


## License

JKFoundation is available under the MIT license. See the LICENSE file for more info.
