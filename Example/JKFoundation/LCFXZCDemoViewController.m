//
//  LCFXZCDemoViewController.m
//  JKFoundation_Example
//
//  Created by admin on 2018/1/27.
//  Copyright © 2018年 iOS11luchao. All rights reserved.
//

#import "LCFXZCDemoViewController.h"

@interface LCFXZCDemoViewController ()

@end

@implementation LCFXZCDemoViewController

- (void)pushRmbHtml {
    self.title = @"奉献支持";
    [[NSUserDefaults standardUserDefaults] setObject:@"4voqn3hl6gqo4ldrxpm4" forKey:@"usercode"];
//15649872106
    //to7lrmha8f4fkh9evcv7
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(228)/255.0 green:(83)/255.0 blue:(77)/255.0 alpha:1]];//设置navbar底色
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];//设置按钮颜色
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont systemFontOfSize:20], NSFontAttributeName,nil]];

    NSString *clzName = @"LCRmbHtmlController";
    Class clz = NSClassFromString(clzName);//将字符串转化成class
    UIViewController *viewController = [[clz alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *string = [NSString stringWithFormat:@"%@",[NNDeviceInformation getDeviceIPAdress]];
    NSLog(@"string = %@", string);
    NSString *simulator = [NSString stringWithFormat:@"%@",[NNDeviceInformation getDeviceName]];
    NSLog(@"simulator = %@", simulator);
    [self pushRmbHtml];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self pushRmbHtml];
}


@end
