//
//  main.m
//  JKFoundation
//
//  Created by iOS11luchao on 12/06/2017.
//  Copyright (c) 2017 iOS11luchao. All rights reserved.
//

@import UIKit;
#import "LCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LCAppDelegate class]));
    }
}
