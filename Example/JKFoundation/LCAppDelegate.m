//
//  LCAppDelegate.m
//  JKFoundation
//
//  Created by iOS11luchao on 12/06/2017.
//  Copyright (c) 2017 iOS11luchao. All rights reserved.
//

#import "LCAppDelegate.h"
#import <Masonry/Masonry.h>


@implementation LCAppDelegate

- (void)testPAWebViewWithName:(NSString *)name {
    
    //    PAWebView *webView = [PAWebView shareInstance];
    PAWebView *webView = [PAWebView shareInstanceWithName:name];
    NSLog(@"webView = %@", webView);
    //加载网页
    [webView loadRequestURL:[NSURL URLWithString:name]];
    [[[UIApplication sharedApplication].delegate window] addSubview:webView.view];
    
    [webView.view mas_makeConstraints:^(MASConstraintMaker *make) {
        // 判断是否是iPhone X
#define iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
        // 状态栏高度
#define STATUS_BAR_HEIGHT (iPhoneX ? 44.f : 20.f)
        // 导航栏高度
#define NAVIGATION_BAR_HEIGHT (iPhoneX ? 88.f : 64.f)
        
        make.top.offset(NAVIGATION_BAR_HEIGHT);
        make.left.right.bottom.offset(0);
    }];
}


- (void)demo1 {
    [self performSelector:@selector(testPAWebViewWithName:) withObject:@"http://buy.9ku.com/apifuyin/shoplogin?userid=1317&time=1516589871904&sign=12c02dd8b8aa5da2c27b27fab9ed7618"/*可传任意类型参数*/ afterDelay:0.4];
    
    
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSString *string = [NSString stringWithFormat:@"%@",@"http://so.com/"];
        [self performSelector:@selector(testPAWebViewWithName:) withObject:string/*可传任意类型参数*/ afterDelay:0.4];
        //        [self performSelector:@selector(testPAWebViewWithName:) withObject:@"LCViewController"/*可传任意类型参数*/ afterDelay:5.4];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            PAWebView *webView = [PAWebView shareInstanceWithName:string];
            [webView.view removeFromSuperview];
        });
    });
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
//    [self demo1];
//    [self setupApplication:@"LCShopWebViewController" ControllerType:0];
    [self setupApplication:@"LCHUDViewController" ControllerType:0];

    return YES;
}

#pragma mark - 配置引导页和主页
-(void)setupApplication:(NSString *)clzName ControllerType:(NSInteger)controllerType{
    Class clz = NSClassFromString(clzName);//将字符串转化成class
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];//windows底色为白色
    if (controllerType == 1) {
        UITabBarController *controller = [[clz alloc] init];
        //引导页判断
        //    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Guide" bundle:nil];
        //    UIViewController *controller = [sb instantiateInitialViewController];
        //    self.window.rootViewController = [self selectRootStr] ? controller :tabBarController;
        //无引导页
        self.window.rootViewController = controller;
    }
    else if (controllerType == 0){
        UIViewController *viewController = [[clz alloc] init];
        //        controller.title = title;
        
        //引导页判断
        //    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Guide" bundle:nil];
        //    UIViewController *controller = [sb instantiateInitialViewController];
        //         self.window.rootViewController = [self selectRootStr] ? controller :viewController;
        
        //无引导页
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController];
        
        self.window.rootViewController = navController;
    }
    
    [self.window makeKeyAndVisible];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
