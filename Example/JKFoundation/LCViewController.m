//
//  LCViewController.m
//  JKFoundation
//
//  Created by iOS11luchao on 12/06/2017.
//  Copyright (c) 2017 iOS11luchao. All rights reserved.
//

#import "LCViewController.h"
#import "JKFoundation.h"

@interface LCViewController ()

@property (weak, nonatomic) IBOutlet UILabel *introducingLabel;

@property (weak, nonatomic) IBOutlet UILabel *htmlLabel;

@property (weak, nonatomic) IBOutlet UILabel *htmlHeightLabel;

@end

@implementation LCViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    BOOL isMobileNumber = [@"17621218888" isMobileNumber];
    NSLog(@"isMobileNumber = %d", isMobileNumber);
    [NSDate lc_GeneratedBuildNumWithDelay:1];
    
    [_introducingLabel settingLabelHeightofRowString:@"源码下载"];
#define RGBColor(r, g, b) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:1]
#define arc4random_color RGBColor(arc4random_uniform(255), arc4random_uniform(255), arc4random_uniform(255))
    _introducingLabel.textColor = arc4random_color;//调用随机色
    _introducingLabel.backgroundColor = arc4random_color;//调用随机色
    _introducingLabel.center = self.view.center;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [_introducingLabel removeFromSuperview];
    
    NSString *htmlStr = @"<div>Tate<span style='color:#1C86EE;'>《WTSDK》</span> <span style='color:#1C86EE;'>Tate_zwt</span> star <span style='color:#FF3E96;'>源码在WTSDK文件夹里，如果你觉得不错的话，麻烦在GitHub上面点个Star，thank you all!";
    [_htmlLabel htmlString:htmlStr];
    _htmlLabel.numberOfLines = 0;
    [_htmlLabel sizeToFit];
    [_htmlLabel tapGesture:^(UIGestureRecognizer *ges) {
        NSLog(@"我被触发了");
    } numberOfTapsRequired:1];
    
    [_htmlHeightLabel htmlString:htmlStr labelRowOfHeight:12];

}

@end
