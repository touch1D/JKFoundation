//
//  LCAppDelegate.h
//  JKFoundation
//
//  Created by iOS11luchao on 12/06/2017.
//  Copyright (c) 2017 iOS11luchao. All rights reserved.
//

@import UIKit;

@interface LCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
