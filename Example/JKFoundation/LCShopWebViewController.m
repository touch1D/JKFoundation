//
//  LCShopWebViewController.m
//  JKFoundation_Example
//
//  Created by admin on 2018/1/31.
//  Copyright © 2018年 iOS11luchao. All rights reserved.
//

#import "LCShopWebViewController.h"
#import <Masonry/Masonry.h>

@interface LCShopWebViewController ()

@end

@implementation LCShopWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSUserDefaults standardUserDefaults] setObject:@"1317" forKey:@"uid"];
    NSString *url = [NSString stringWithFormat:@"%@?%@",@"http://buy.9ku.com/apifuyin/shoplogin",[self getUrlStrMove]];

    [self performSelector:@selector(testPAWebViewWithName:) withObject:url/*可传任意类型参数*/ afterDelay:0.4];
}
- (NSString *)getUrlStrMove {
    NSString *key = @"#lskdflkas(*(jfkl214lkl,.,.mj";
    
    NSDate *dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval a = [dat timeIntervalSince1970];
    NSString *timeString = [NSString stringWithFormat:@"%.0f", a * 1000];
    
    NSString * uid  = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"uid"]];
    //     NSString * uid  = @"100";
    NSString *sign = [[NSString stringWithFormat:@"userid=%@&time=%@%@",uid,timeString,key] MD5];
    
    return [NSString stringWithFormat:@"userid=%@&time=%@&sign=%@",uid,timeString,sign];
}

- (void)testPAWebViewWithName:(NSString *)name {
    
    //    PAWebView *webView = [PAWebView shareInstance];
    PAWebView *webView = [PAWebView shareInstanceWithName:name];
    webView.link = name;
//    NSLog(@"webView = %@", webView);
    //加载网页
    [webView loadRequestURL:[NSURL URLWithString:name]];
    [[[UIApplication sharedApplication].delegate window] addSubview:webView.view];
    
    [webView.view mas_makeConstraints:^(MASConstraintMaker *make) {
        // 判断是否是iPhone X
#define iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
        // 状态栏高度
#define STATUS_BAR_HEIGHT (iPhoneX ? 44.f : 20.f)
        // 导航栏高度
#define NAVIGATION_BAR_HEIGHT (iPhoneX ? 88.f : 64.f)
        
        make.top.offset(NAVIGATION_BAR_HEIGHT);
        make.left.right.bottom.offset(0);
    }];
}

@end
