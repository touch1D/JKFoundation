//
//  LCHUDViewController.m
//  JKFoundation_Example
//
//  Created by admin on 2018/2/9.
//  Copyright © 2018年 iOS11luchao. All rights reserved.
//

#import "LCHUDViewController.h"

@interface LCHUDViewController ()

@end

@implementation LCHUDViewController

- (void)dealloc {
    NSLog(@"__func__dealloc = %s", __func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self showSuccessMessage:@"访问成功"];
//    [self showErrorMessage:@"网络错误"];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self pushVC];
}

- (void)pushVC {
    NSString *clzName = @"LCHUDViewController";
    Class clz = NSClassFromString(clzName);//将字符串转化成class
    UIViewController *viewController = [[clz alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
