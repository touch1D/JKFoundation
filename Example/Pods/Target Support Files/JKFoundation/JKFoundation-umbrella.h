#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "JKFoundation.h"
#import "LCSingleton.h"
#import "NNDeviceInformation.h"
#import "NSDate+LCAddition.h"
#import "NSString+LCAddition.h"
#import "NSURL+PATool.h"
#import "PAWebView+UIDelegate.h"
#import "PAWebView.h"
#import "NSObject+PARuntime.h"
#import "WKWebView+PAWebCache.h"
#import "WKWebView+PAWebCookie.h"
#import "PAWebNavigationController.h"
#import "PAWKPanGestureRecognizer.h"
#import "UIAlertController+WKWebAlert.h"
#import "WKBaseWebView.h"
#import "registerURLSchemes.h"
#import "urlschemeModel.h"
#import "WKScanQRCode.h"
#import "NSArray+WT.h"
#import "NSDate+WT.h"
#import "NSString+WT.h"
#import "NSTimer+WT.h"
#import "CALayer+WT.h"
#import "UIBarButtonItem+WT.h"
#import "UIButton+WT.h"
#import "UIImage+WT.h"
#import "UILabel+WT.h"
#import "UITextView+WT.h"
#import "UIView+WT.h"
#import "UIViewController+WT.h"
#import "WTConst.h"
#import "WTUtility.h"
#import "WTLabel.h"
#import "WTTextField.h"
#import "WTTextView.h"

FOUNDATION_EXPORT double JKFoundationVersionNumber;
FOUNDATION_EXPORT const unsigned char JKFoundationVersionString[];

